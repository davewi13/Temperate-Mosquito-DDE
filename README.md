# Temperate-Mosquito-DDE

For code used in the paper "Modelling the Effects of Temperature on Temperature Mosquito Seasonal Abundance" please see file Temperate_mosquito_DDE_code.f90.

For code used in the paper "Uncovering mechanisms behind mosquito seasonality by integrating mathematical models and daily empirical population data: Culex pipiens in the UK" please see the file Ewing et al 2018 - Data paper.f90.

For code used in my PhD please see the file named by the relevant chapter number.

The DDE solver code can be found at http://www.radford.edu/~thompson/ffddes/
